FROM ghcr.io/kiwix/kiwix-tools:3.7.0-1 AS kiwix-tools
FROM ubuntu:22.04 AS base

COPY --from=kiwix-tools /usr/local/bin/kiwix-manage /usr/local/bin
COPY --from=kiwix-tools /usr/local/bin/kiwix-serve /usr/local/bin

COPY entrypoint.sh /
RUN chmod +x entrypoint.sh

EXPOSE 8080

ENTRYPOINT [ "/entrypoint.sh" ]

CMD ["/usr/local/bin/kiwix-serve", "--library", "/data/library.xml", "--port", "8080"]